function Menu(menuBtnId, menuListId, menuListCollectionClass, menuBtnPressedClass, menuListVisibleClass) {

    var menuButton = document.getElementById(menuBtnId);
    var menuList = document.getElementById(menuListId);
    var menuListCollection = menuList.getElementsByClassName(menuListCollectionClass);

    menuButton.addEventListener('click', function(event) {

        menuButton.classList.toggle(menuBtnPressedClass);
        menuList.classList.toggle(menuListVisibleClass);
        
        event.stopPropagation();
        
        document.addEventListener('click', function menuActivated(event) {
            menuButton.classList.remove(menuBtnPressedClass);
            menuList.classList.remove(menuListVisibleClass);
            document.removeEventListener('click', menuActivated);
        });
    });


    this.reverseMenu = function reverseMenu() {

        for (var i = 0; i < menuListCollection.length; i++) {
            menuList.appendChild(menuListCollection[menuListCollection.length-1-i]);
        }
        menuList.appendChild(menuListCollection[0]);
    }

    if (document.documentElement.clientWidth > 999) {
        this.reverseMenu();
    }
}