function Parallax(parallaxItemId, speed) {

    var self = this;
    var $parallaxItem = $('#' + parallaxItemId);
    var startPosition = parseInt($parallaxItem.css('top'));

    if ($parallaxItem.has('[data-start]')) {
        $parallaxItem.css('top', ($parallaxItem.parent().offset().top) * -speed + 'px');
        startPosition = parseInt($parallaxItem.css('top'));
    }

    this.parallaxScroll = function () {
        $parallaxItem.css('top', (startPosition + ($(window).scrollTop() * speed)) + 'px');
    }

    $(window).bind('scroll', function (e) {
        self.parallaxScroll();
    });
}
