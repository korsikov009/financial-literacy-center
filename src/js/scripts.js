/**
 * jQuery Spincrement plugin
 *
 * Plugin structure based on: http://blog.jeremymartin.name/2008/02/building-your-first-jquery-plugin-that.html
 * Leveraging of jQuery animate() based on: http://www.bennadel.com/blog/2007-Using-jQuery-s-animate-Method-To-Power-Easing-Based-Iteration.htm
 * Easing function from jQuery Easing plugin: http://gsgd.co.uk/sandbox/jquery/easing/
 * Thousands separator code: http://www.webmasterworld.com/forum91/8.htm
 *
 * @author John J. Camilleri
 * @version 1.2
 */

/* global jQuery */

(function ($) {
  // Custom easing function
  $.extend($.easing, {
    // This is ripped directly from the jQuery easing plugin (easeOutExpo), from: http://gsgd.co.uk/sandbox/jquery/easing/
    spincrementEasing: function (x, t, b, c, d) {
      return (t === d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b
    }
  })

  // Spincrement function
  $.fn.spincrement = function (opts) {
    // Default values
    var defaults = {
      from: 0,
      to: null,
      decimalPlaces: null,
      decimalPoint: '.',
      thousandSeparator: ',',
      duration: 1000, // ms; TOTAL length animation
      leeway: 50, // percent of duraion
      easing: 'spincrementEasing',
      fade: true,
      complete: null
    }
    var options = $.extend(defaults, opts)

    // Function for formatting number
    var re_thouSep = new RegExp(/^(-?[0-9]+)([0-9]{3})/)
    function format (num, dp) {
      num = num.toFixed(dp) // converts to string!

      // Non "." decimal point
      if ((dp > 0) && (options.decimalPoint !== '.')) {
        num = num.replace('.', options.decimalPoint)
      }

      // Thousands separator
      if (options.thousandSeparator) {
        while (re_thouSep.test(num)) {
          num = num.replace(re_thouSep, '$1' + options.thousandSeparator + '$2')
        }
      }
      return num
    }

    // Apply to each matching item
    return this.each(function () {
      // Get handle on current obj
      var obj = $(this)

      // Set params FOR THIS ELEM
      var from = options.from
      if (obj.attr('data-from')) {
        from = parseFloat(obj.attr('data-from'))
      }

      var to
      if (obj.attr('data-to')) {
        to = parseFloat(obj.attr('data-to'))
      } else if (options.to !== null) {
        to = options.to
      } else {
        var ts = $.inArray(options.thousandSeparator, ['\\', '^', '$', '*', '+', '?', '.']) > -1 ? '\\' + options.thousandSeparator : options.thousandSeparator
        var re = new RegExp(ts, 'g')
        to = parseFloat(obj.text().replace(re, ''))
      }

      var duration = options.duration
      if (options.leeway) {
        // If leeway is set, randomise duration a little
        duration += Math.round(options.duration * ((Math.random() * 2) - 1) * options.leeway / 100)
      }

      var dp
      if (obj.attr('data-dp')) {
        dp = parseInt(obj.attr('data-dp'), 10)
      } else if (options.decimalPlaces !== null) {
        dp = options.decimalPlaces
      } else {
        var ix = obj.text().indexOf(options.decimalPoint)
        dp = (ix > -1) ? obj.text().length - (ix + 1) : 0
      }

      // Start
      obj.css('counter', from)
      if (options.fade) obj.css('opacity', 0)
      obj.animate(
        {
          counter: to,
          opacity: 1
        },
        {
          easing: options.easing,
          duration: duration,

          // Invoke the callback for each step.
          step: function (progress) {
            obj.html(format(progress * to, dp))
          },
          complete: function () {
            // Cleanup
            obj.css('counter', null)
            obj.html(format(to, dp))

            // user's callback
            if (options.complete) {
              options.complete(obj)
            }
          }
        }
      )
    })
  }
})(jQuery);

function Menu(menuBtnId, menuListId, menuListCollectionClass, menuBtnPressedClass, menuListVisibleClass) {

    var menuButton = document.getElementById(menuBtnId);
    var menuList = document.getElementById(menuListId);
    var menuListCollection = menuList.getElementsByClassName(menuListCollectionClass);

    menuButton.addEventListener('click', function(event) {

        menuButton.classList.toggle(menuBtnPressedClass);
        menuList.classList.toggle(menuListVisibleClass);
        
        event.stopPropagation();
        
        document.addEventListener('click', function menuActivated(event) {
            menuButton.classList.remove(menuBtnPressedClass);
            menuList.classList.remove(menuListVisibleClass);
            document.removeEventListener('click', menuActivated);
        });
    });


    this.reverseMenu = function reverseMenu() {

        for (var i = 0; i < menuListCollection.length; i++) {
            menuList.appendChild(menuListCollection[menuListCollection.length-1-i]);
        }
        menuList.appendChild(menuListCollection[0]);
    }

    if (document.documentElement.clientWidth > 999) {
        this.reverseMenu();
    }
}
function Parallax(parallaxItemId, speed) {

    var self = this;
    var $parallaxItem = $('#' + parallaxItemId);
    var startPosition = parseInt($parallaxItem.css('top'));

    if ($parallaxItem.has('[data-start]')) {
        $parallaxItem.css('top', ($parallaxItem.parent().offset().top) * -speed + 'px');
        startPosition = parseInt($parallaxItem.css('top'));
    }

    this.parallaxScroll = function () {
        $parallaxItem.css('top', (startPosition + ($(window).scrollTop() * speed)) + 'px');
    }

    $(window).bind('scroll', function (e) {
        self.parallaxScroll();
    });
}

function Slider(viewportId, slideWrapperId, leftButtonClass, rightButtonClass, buttonDisableClass) {

    var $leftButton = $('.' + leftButtonClass),
        $rightButton = $('.' + rightButtonClass),
        $viewport = $('#' + viewportId),
        $slideWrapper = $('#' + slideWrapperId);
    
    
    var slideNow = 1;
    var slideNumber = 2;
    var slideCount;
    

    if ($(window).width() < 1000) {
        slideNumber = 1;
        slideCount = $slideWrapper.children().length;
        $slideWrapper.css({'width': 100 * slideCount + '%'});
    }
    else {
        slideNumber = 2;
        slideCount = $slideWrapper.children().length - 1;
        $slideWrapper.css({'width': (100 * (slideCount + 1))/2 + '%'});

    }

    this.prevSlide = function () {
        if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
            slideNow = 1;
        } else {
            var translateWidth = -$viewport.width()/slideNumber * (slideNow - 2)
            $slideWrapper.css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow--;
            if (slideNow <= 1) {
                $leftButton.addClass(buttonDisableClass);
            } else {
                $rightButton.removeClass(buttonDisableClass);
            }
        }
    }

    this.nextSlide = function () {
        if (slideNow == slideCount || slideNow > slideCount) {
            slideNow = slideCount;
        } else {
            var translateWidth = -$viewport.width()/slideNumber * (slideNow);
            $slideWrapper.css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow++;

            if (slideNow >= slideCount) {
                $rightButton.addClass(buttonDisableClass);
            } else {
                $leftButton.removeClass(buttonDisableClass);
            }
        }
    }
    
    $leftButton.click(this.prevSlide);
    $rightButton.click(this.nextSlide);
}
function indicationCoins() {
	var show = true;
	var countbox = "#counts";
	$(window).on("scroll load resize", function(){
		if(!show) return false;                   // Отменяем показ анимации, если она уже была выполнена
		var w_top = $(window).scrollTop();        // Количество пикселей на которое была прокручена страница
		var e_top = $(countbox).offset().top;     // Расстояние от блока со счетчиками до верха всего документа
		var w_height = $(window).height();        // Высота окна браузера
		var d_height = $(document).height();      // Высота всего документа
		var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
		if (w_top + e_height >= e_top || e_height + e_top < w_height) {
			$(".spincrement").spincrement({
				from: false,
				thousandSeparator: "",
				duration: 1500
			});
			show = false;
		}
	});
}
$(document).ready(function () {
    var menu = new Menu('menu__btn', 'menu__list', 'menu__li', 'menu__btn_press_pressed', 'menu__list_visibility_visible');
    var firstParallax = new Parallax('application__parallax', 0.4);
    var secondParallax = new Parallax('application-info__parallax', 0.4);
    var slider = Slider('viewport', 'slidewrapper', 'reviews__btn-left', 'reviews__btn-right', 'reviews__btn_disable');
    indicationCoins();
});