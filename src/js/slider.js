function Slider(viewportId, slideWrapperId, leftButtonClass, rightButtonClass, buttonDisableClass) {

    var $leftButton = $('.' + leftButtonClass),
        $rightButton = $('.' + rightButtonClass),
        $viewport = $('#' + viewportId),
        $slideWrapper = $('#' + slideWrapperId);
    
    
    var slideNow = 1;
    var slideNumber = 2;
    var slideCount;
    

    if ($(window).width() < 1000) {
        slideNumber = 1;
        slideCount = $slideWrapper.children().length;
        $slideWrapper.css({'width': 100 * slideCount + '%'});
    }
    else {
        slideNumber = 2;
        slideCount = $slideWrapper.children().length - 1;
        $slideWrapper.css({'width': (100 * (slideCount + 1))/2 + '%'});

    }

    this.prevSlide = function () {
        if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
            slideNow = 1;
        } else {
            var translateWidth = -$viewport.width()/slideNumber * (slideNow - 2)
            $slideWrapper.css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow--;
            if (slideNow <= 1) {
                $leftButton.addClass(buttonDisableClass);
            } else {
                $rightButton.removeClass(buttonDisableClass);
            }
        }
    }

    this.nextSlide = function () {
        if (slideNow == slideCount || slideNow > slideCount) {
            slideNow = slideCount;
        } else {
            var translateWidth = -$viewport.width()/slideNumber * (slideNow);
            $slideWrapper.css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow++;

            if (slideNow >= slideCount) {
                $rightButton.addClass(buttonDisableClass);
            } else {
                $leftButton.removeClass(buttonDisableClass);
            }
        }
    }
    
    $leftButton.click(this.prevSlide);
    $rightButton.click(this.nextSlide);
}