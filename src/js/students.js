$(document).ready(function () {
    var menu = new Menu('menu__btn', 'menu__list', 'menu__li', 'menu__btn_press_pressed', 'menu__list_visibility_visible');
    var firstParallax = new Parallax('application__parallax', 0.4);
    var secondParallax = new Parallax('application-info__parallax', 0.4);
    var slider = Slider('viewport', 'slidewrapper', 'reviews__btn-left', 'reviews__btn-right', 'reviews__btn_disable');
    indicationCoins();
});